package api

import (
  "net/http"

  auth "github.com/supersoccer/go-auth-api/authpassport/v1/authpassport"
  "github.com/supersoccer/gojunkyard/http/httpresponse"
  "github.com/supersoccer/gojunkyard/router"
)

// Config type
type Config struct {
  Server  string
  AuthURL string
}

// API type
type API struct {
  Config Config
}

// NewAPI ...
func NewAPI() *API {
  config := &Config{
    Server:  "localhost",
    AuthURL: "http://localhost:9000",
  }
  return &API{
    Config: *config,
  }
}

// Register router
func (api *API) Register(router *router.Router) {
  router.Get("/orders/order/:id", auth.AuthPass(api.GetOrderByID, "https://api.supersoccer.tv/orders/orders.read", api.Config.AuthURL))
  router.Get("/orders/orderid/:id", auth.AuthPass(api.GetOrderByOrderID, "https://api.supersoccer.tv/orders/orders.read", api.Config.AuthURL))
  router.Get("/orders/orders", auth.AuthPass(api.GetOrders, "https://api.supersoccer.tv/orders/orders.read", api.Config.AuthURL))
  router.Post("/orders/create", auth.AuthPass(api.CreateOrder, "https://api.supersoccer.tv/orders/orders.create", api.Config.AuthURL))
  router.Post("/orders/update", auth.AuthPass(api.UpdateOrder, "https://api.supersoccer.tv/orders/orders.update", api.Config.AuthURL))
  router.Del("/orders/order/:id", auth.AuthPass(api.DeleteOrder, "https://api.supersoccer.tv/orders/orders.delete", api.Config.AuthURL))
}

// GetOrderByID ...
func (api *API) GetOrderByID(w http.ResponseWriter, r *http.Request) {
  httpresponse.WithData(w, "getOrderById")
}

// GetOrderByOrderID ...
func (api *API) GetOrderByOrderID(w http.ResponseWriter, r *http.Request) {
  httpresponse.WithData(w, "getOrderByOrderId")
}

// GetOrders ...
func (api *API) GetOrders(w http.ResponseWriter, r *http.Request) {
  httpresponse.WithData(w, "GetOrders")
}

// CreateOrder ...
func (api *API) CreateOrder(w http.ResponseWriter, r *http.Request) {
  httpresponse.WithData(w, "createOrder")
}

// UpdateOrder ...
func (api *API) UpdateOrder(w http.ResponseWriter, r *http.Request) {
  httpresponse.WithData(w, "UpdateOrder")
}

// DeleteOrder ...
func (api *API) DeleteOrder(w http.ResponseWriter, r *http.Request) {
  httpresponse.WithData(w, "DeleteOrder")
}
