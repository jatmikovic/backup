APP_NAME="orders-api"
GODEP="dep"

get-deps:
	@echo "${NOW} GETTING DEPENDENCIES..."
	@${GODEP} ensure -v

run:
	go run main.go

build:
	go build -o ${APP_NAME} main.go
