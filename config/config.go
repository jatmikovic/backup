package config

// Config stores application configurations.
type Config struct {
  Server       ServerConfig     `envconfig:"SERVER"`
  Oauth        pkg.OauthData    `envconfig:"OAUTH"`
  APIURL       pkg.APIURL       `envconfig:"API"`
  ClientID     string           `envconfig:"CLIENT_ID"`
  SecretKey    string           `envconfig:"SECRET_KEY"`
  Database     DatabaseConfig   `envconfig:"DATABASE"`
  Domain       DomainConfig     `envconfig:"DOMAIN"`
  UserRedis    UserRedisConfig  `envconfig:"REDIS_USER"`
  OrderRedis   OrderRedisConfig `envconfig:"REDIS_ORDER"`
  UserPrefixes pkg.UserPrefixes `envconfig:"APPS_REDPREF"`
  Prefixes     pkg.Prefixes     `envconfig:"APPS_REDPREF"`
  Sentry       SentryConfig     `envconfig:"SENTRY"`
}
