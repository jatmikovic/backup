package main

import (
  "context"
  "time"

  api "git.sstv.io/apps/supersoccer/api/orders/web/api/v1"
  log "github.com/sirupsen/logrus"
  "github.com/supersoccer/gojunkyard/webserver"
)

func main() {
  webOptions := &webserver.Options{
    ListenAddress:  "localhost:3550",
    MaxConnections: 512,
    ReadTimeout:    10 * time.Second,
    SentryClient:   nil,
  }

	// Initializes web handler
  webSrv := webserver.New(webOptions)

	apiWeb := *api.NewAPI()

  apiWeb.Register(webSrv.Router().WithPrefix("/api/v1"))

	if err := webSrv.Run(context.Background()); err != nil {
		log.Panic(err)
  }
}
